package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io"
	"time"

	protobufpb "grpcwallet/src/protobuf/protobufpb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func main() {
	fmt.Println("Wallet Client")

	var host = "localhost:8080"

	var opts []grpc.DialOption
	if host != "" {
		opts = append(opts, grpc.WithAuthority(host))
	}
	systemRoots, err := x509.SystemCertPool()
	if err != nil {
		fmt.Println("Error in system certpool", err)
	}
	cred := credentials.NewTLS(&tls.Config{
		RootCAs: systemRoots,
	})
	opts = append(opts, grpc.WithTransportCredentials(cred))

	cc, err := grpc.Dial(host, opts...)
	if err != nil {
		fmt.Println("dial error", err)
	}
	//ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	// cc, err := grpc.DialContext(ctx,host,opts...)
	// if err != nil {
	// 	fmt.Println("dial error", err)
	// }
	//cc, err := grpc.Dial(host, grpc.WithInsecure())

	c := protobufpb.NewWalletServiceClient(cc)

	doUnaryCredit(c)

}

func doUnaryCredit(c protobufpb.WalletServiceClient) {

	stream, err := c.DebitWallet(context.Background())

	if err != nil {
		fmt.Println("conn err", err)
	}

	var final []*protobufpb.Request

	for i := 901; i < 1401; i++ {

		r := &protobufpb.Request{
			//Walletledgerid:    640843258153795585,
			Amount:      1,
			Walletid:    487,
			Apiwalletid: 3,
			//Apiwalletledgerid: 640843258358267905,
			Txnid: int64(i),
			IsSl:  false,
			//GatewayTxnID:    1234567,
			//StatusCode:      0,
			//Ispartialrefund: true,
			TransactionType: "Hello",
		}
		final = append(final, r)
	}

	waitc := make(chan struct{})

	go func() {

		for _, req := range final {
			fmt.Println("Sending Message:", req)
			//fmt.Println(time.Now().Format("2006-01-02 15:04:05.000"))
			stream.Send(req)
			//time.Sleep(1000 * time.Millisecond)
		}
		stream.CloseSend()

	}()

	go func() {
		start := time.Now()
		for {
			start := time.Now()
			res, err := stream.Recv()

			if err == io.EOF {
				break
			}
			if err != nil {
				fmt.Print("rcv err", err)
				break
			}
			//fmt.Println(time.Now().Format("2006-01-02 15:04:05.000"))
			fmt.Println("Per req", time.Since(start))
			fmt.Printf("Received: %v\n ", res)
		}
		fmt.Println(time.Since(start))
		//close(waitc)
	}()

	<-waitc

}
