package walletoperation

import (
	"context"
	"errors"
	"fmt"
	"grpcwallet/walletservice"
	"time"

	"github.com/jackc/pgx/v4"
)

func updatewalletdebit(ctx context.Context, tx pgx.Tx, debitreq walletservice.Walletperform) error {
	issl := debitreq.Is_sl
	fmt.Println("Txn id in request for update debit: ", debitreq.Id, "Wallet ID:", debitreq.WalletId, "Api Wallet ID:", debitreq.ApiWalletId, "of amount:", debitreq.Amount, "Getting user transaction layer: ", debitreq.Is_sl)
	// fmt.Println("Txn id in request for update debit: ", debitreq.Id, "of amount:", debitreq.Amount)
	// fmt.Println("Txn id in request for update debit: ", debitreq.Id, "Getting user transaction layer: ", debitreq.Is_sl)
	if issl && debitreq.ApiWalletId != 0 {
		select1 := time.Now()
		if err := tx.QueryRow(ctx,
			"SELECT balance,minimum_balance FROM wallet_api WHERE id = $1 FOR UPDATE", debitreq.ApiWalletId).Scan(&debitreq.ApiPreviousBalance, &debitreq.ApiminimumBalance); err != nil {
			return err
		}
		select1End := time.Since(select1)
		fmt.Println("api wallet debit select time", select1End)
		if debitreq.ApiPreviousBalance < debitreq.Amount {
			return errors.New("8")
		} else if debitreq.ApiPreviousBalance-debitreq.Amount < debitreq.ApiminimumBalance {
			return errors.New("6")
		}
		update1 := time.Now()
		if _, err := tx.Exec(ctx,
			"UPDATE wallet_api SET balance = balance - $1 WHERE id = $2", debitreq.Amount, debitreq.ApiWalletId); err != nil {
			return err
		}
		update1End := time.Since(update1)
		fmt.Println("api wallet debit update time", update1End)

		debitreq.ApiCurrentBalance = debitreq.ApiPreviousBalance - debitreq.Amount
		debitreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		updateledg1 := time.Now()
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_api_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate = $5 where id = $1", debitreq.ApiId, debitreq.ApiPreviousBalance, debitreq.ApiCurrentBalance, "SUCCESS", debitreq.UpdatedDate); err != nil {
			return err
		}
		updateledg1end := time.Since(updateledg1)
		fmt.Println("ledger update for api wallet update time", updateledg1end)
		debitreq.Status = "SUCCESS"
		go walletservice.PubsubData(debitreq)
		return nil
	} else if issl && debitreq.WalletId != 0 {
		select2 := time.Now()
		if err := tx.QueryRow(ctx,
			"SELECT balance,minimum_balance FROM wallet WHERE id = $1 FOR UPDATE", debitreq.WalletId).Scan(&debitreq.PreviousBalance, &debitreq.MinimumBalance); err != nil {
			return err
		}
		select1End := time.Since(select2)
		fmt.Println("api weallet debit time", select1End)
		if debitreq.PreviousBalance < debitreq.Amount {
			return errors.New("7")
		} else if debitreq.PreviousBalance-debitreq.Amount < debitreq.MinimumBalance {
			return errors.New("5")
		}
		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance - $1 WHERE id = $2", debitreq.Amount, debitreq.WalletId); err != nil {
			return err
		}
		debitreq.CurrentBalance = debitreq.PreviousBalance - debitreq.Amount
		debitreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate=$5 where id = $1", debitreq.Id, debitreq.PreviousBalance, debitreq.CurrentBalance, "SUCCESS", debitreq.UpdatedDate); err != nil {
			return err
		}
		debitreq.Status = "SUCCESS"
		go walletservice.PubsubData(debitreq)
		return nil
	} else if !issl && debitreq.WalletId != 0 && debitreq.ApiWalletId != 0 {
		selct1 := time.Now()
		if err := tx.QueryRow(ctx,
			"SELECT balance,minimum_balance FROM wallet WHERE id = $1 FOR UPDATE", debitreq.WalletId).Scan(&debitreq.PreviousBalance, &debitreq.MinimumBalance); err != nil {
			return err
		}
		select1end := time.Since(selct1)
		fmt.Println("total time for select query for wallet ", select1end, "for", debitreq.TxnId)
		selct2 := time.Now()
		if err := tx.QueryRow(ctx,
			"SELECT balance,minimum_balance FROM wallet_api WHERE id = $1 FOR UPDATE", debitreq.ApiWalletId).Scan(&debitreq.ApiPreviousBalance, &debitreq.ApiminimumBalance); err != nil {
			return err
		}
		select2end := time.Since(selct2)
		fmt.Println("total time for select query for wallet api ", select2end, "for", debitreq.TxnId)
		if debitreq.PreviousBalance < debitreq.Amount {
			return errors.New("7")
		} else if debitreq.ApiPreviousBalance < debitreq.Amount {
			return errors.New("8")
		} else if (debitreq.PreviousBalance - debitreq.Amount) < debitreq.MinimumBalance {
			return errors.New("5")
		} else if (debitreq.ApiPreviousBalance - debitreq.Amount) < debitreq.ApiminimumBalance {
			return errors.New("6")
		}
		update1 := time.Now()
		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance - $1 WHERE id = $2", debitreq.Amount, debitreq.WalletId); err != nil {
			return err
		}
		update1end := time.Since(update1)
		fmt.Println("total time for update query for wallet ", update1end, "for", debitreq.TxnId)
		debitreq.CurrentBalance = debitreq.PreviousBalance - debitreq.Amount
		debitreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		update2 := time.Now()
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate = $5 where id = $1", debitreq.Id, debitreq.PreviousBalance, debitreq.CurrentBalance, "SUCCESS", debitreq.UpdatedDate); err != nil {
			fmt.Println("Exception in wallet ledger data ")
			return err
		}
		update2end := time.Since(update2)

		fmt.Println("total time for update query for wallet ledger ", update2end, "for", debitreq.TxnId)
		update3 := time.Now()
		if _, err := tx.Exec(ctx,
			"UPDATE wallet_api SET balance = balance - $1 WHERE id = $2", debitreq.Amount, debitreq.ApiWalletId); err != nil {
			return err
		}
		update3end := time.Since(update3)
		fmt.Println("total time for update query for wallet api ", update3end, "for", debitreq.TxnId)
		debitreq.ApiCurrentBalance = debitreq.ApiPreviousBalance - debitreq.Amount
		update4 := time.Now()
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_api_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate = $5 where id = $1", debitreq.ApiId, debitreq.ApiPreviousBalance, debitreq.ApiCurrentBalance, "SUCCESS", debitreq.UpdatedDate); err != nil {
			return err
		}
		update4end := time.Since(update4)
		fmt.Println("total time for update query for wallet api ledger  ", update4end, "for", debitreq.TxnId)
		debitreq.Status = "SUCCESS"

		go walletservice.PubsubData(debitreq)
		return nil
	} else {
		return errors.New("wrong data")
	}

}
