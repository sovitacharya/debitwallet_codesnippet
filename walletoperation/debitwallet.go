package walletoperation

import (
	"context"
	"fmt"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
	"grpcwallet/walletservice"
	"io"
	"time"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Server struct {
	Conn *pgxpool.Pool
}

var AcquiredCount int

func (s *Server) DebitWallet(stream protobufpb.WalletService_DebitWalletServer) error {
	fmt.Println("Inside Debitwallet")
	for {
		//stream data received for debit wallet
		req, err := stream.Recv()
		if err==io.EOF {
			//If stream data receive nil in request break this stream
			fmt.Println("client done streamming",req)
			fmt.Println("Inside Debit wallet, request is", err)
			return nil
		} else if err != nil {
			//If error occured in receiving stream data
			fmt.Println("Inside Debitwallet::Error in stream receive:", err)
			continue
		}
		startTime := time.Now()
		debitreq := walletservice.Walletperform{}

		fmt.Println("Txn id in request for debit: ", req.Txnid, "Wallet ID:", req.Walletid, "Api Wallet ID:", req.Apiwalletid)
		fmt.Println("Txn id in request for debit: ", req.Txnid, "of amount:", req.Amount)
		fmt.Println("Txn id in request for debit: ", req.Txnid, "Getting user transaction layer: ", req.IsSl, "Txn Type", req.TransactionType)
		//Check for Requested data is valid or not

		response, checkerr := walletservice.Checkvalidationforwallet(req)
		if checkerr != nil {
			sendErr := stream.Send(response)
			if sendErr != nil {
				fmt.Println("Inside Deitwallet::Stream send error::", req.Txnid, ":Failure desc:", sendErr)
				continue
			}
		} else {
			//Request Prepartion for debit wallet
			debitreq.Is_sl = req.IsSl
			debitreq.WalletId = req.Walletid
			debitreq.ApiWalletId = req.Apiwalletid
			debitreq.Amount = req.Amount
			debitreq.TxnId = req.Txnid
			debitreq.Type = 1 //For Debit
			debitreq.TransactionType = req.TransactionType
			//Conn acquire for insert
			acq1 := time.Now()
			conn, err := s.Conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Txn id in request for debit::", req.Txnid, "Error in conn acquire in insert :", err.Error())
			}
			AcquiredCount++
			acq1end := time.Since(acq1)
			fmt.Println("Time For Acquiring Connection Before Initiate", acq1end, "for", req.Txnid)
			//insert service for initiated transaction of wallet ledger
			initiateStart := time.Now()
			debitreq, err1 := walletservice.InsertWalletledger(context.Background(), conn, debitreq)
			endinitiate := time.Since(initiateStart)
			fmt.Println("Total  Time for Inititate", endinitiate, "for", req.Txnid)
			//Conn release
			conn.Release()
			AcquiredCount--
			if err1 == nil {
				//If insert data in ledger success
				acq2 := time.Now()
				conn, err := s.Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Txn id in request for debit::", req.Txnid, "Error in conn acquire in update :", err.Error())
				}
				AcquiredCount++
				acq2end := time.Since(acq2)
				fmt.Println("Total Time Taken For Connection Acquire before update", acq2end, "for", req.Txnid)
				updatestart := time.Now()
				err = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {
					conn.Exec(context.Background(), "SET TRANSACTION PRIORITY HIGH")
					fmt.Println("coming for update")
					err := updatewalletdebit(context.Background(), tx, debitreq)
					return err
				})
				endupdate := time.Since(updatestart)
				fmt.Println("total update time", endupdate)
				conn.Release()
				AcquiredCount--
				if err == nil {
					response.Txnid = req.Txnid
					response.Walletstatus = "SUCCESS"
					response.Walletstatusdesc = "Successfully Debited"
					response.Wallettransactionid = debitreq.Id
					response.Apiwallettransactionid = debitreq.ApiId
					response.Walletstatuscode = 0
					fmt.Println("Txn id in request for debit: ", req.Txnid, "status is :", response.Walletstatus)
					endtime := time.Since(startTime)
					fmt.Println("total time for transaction", endtime, "for", req.Txnid)

				} else if err != nil {
					fmt.Println("Txn id in request for debit: ", req.Txnid, "exception in update :", err)
					response.Txnid = req.Txnid
					response.Wallettransactionid = debitreq.Id
					response.Apiwallettransactionid = debitreq.ApiId
					response.Walletstatus = "FAILED"
					switch err.Error() {
					case "5":
						// response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Minimum balance not available"
						response.Walletstatuscode = 3
					case "6":
						// response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "API minimum balance not available"
						response.Walletstatuscode = 3
					case "7":
						// response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Balance not available"
						response.Walletstatuscode = 4
					case "8":
						// response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "API balance not available"
						response.Walletstatuscode = 4
					case "9":
						// response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Maximum Balnce Limit exceed of API user"
						response.Walletstatuscode = 5
					case "10":
						// response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Maximum Balance Limit exceed of user"
						response.Walletstatuscode = 5
					default:
						// response.Walletstatus = "FAILED"
						response.Walletstatusdesc = err.Error()
						response.Walletstatuscode = 7
					}

					debitreq.Status = response.Walletstatus
					debitreq.StatusCode = response.Walletstatuscode
					acq3 := time.Now()

					conn, err := s.Conn.Acquire(context.Background())
					if err != nil {
						fmt.Println("Txn id in request for debit::", req.GetTxnid(), "Error in conn acquire in update :", err.Error())
					}
					AcquiredCount++
					acq3end := time.Since(acq3)
					fmt.Println("Time For Acquiring Conn Before update Status Code for failed case ", acq3end, "for", req.Txnid)
					walletservice.UpdateStatusCode(debitreq, conn)
					sendErr := stream.Send(response)
					if sendErr != nil {
						fmt.Println("Inside Debitwallet::Stream send error::", response.Txnid, ":Failure desc:", sendErr)
						continue
					}
					conn.Release()
					AcquiredCount--
					continue

				}
				fmt.Println("Acquired Count for Connection", AcquiredCount)
				sendErr := stream.Send(response)
				if sendErr != nil {
					fmt.Println("Inside Debitwallet::Stream send error::", response.Txnid, ":Failure desc:", sendErr)
					continue
				}

			} else if err1 != nil {
				response.Txnid = req.Txnid
				response.Wallettransactionid = debitreq.Id
				response.Apiwallettransactionid = debitreq.ApiId
				response.Walletstatus = "FAILED"
				switch err1.Error() {
				case "1":
					// response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "API Wallet ID is not available"
					response.Walletstatuscode = 2
				case "2":
					// response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "Wallet ID is not available"
					response.Walletstatuscode = 2
				case "3":
					// response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "Wallet ID is not active"
					response.Walletstatuscode = 1
				case "4":
					// response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "API wallet ID is not active"
					response.Walletstatuscode = 1
				case "5":
					// response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "Wallet ID/API WalletID is not active"
					response.Walletstatuscode = 1
				default:
					// response.Walletstatus = "FAILED"
					response.Walletstatusdesc = err1.Error()
					response.Walletstatuscode = 6
				}

				sendErr := stream.Send(response)
				if sendErr != nil {
					fmt.Println("Inside Debitwallet::Stream send error::", response.Txnid, ":Failure desc:", sendErr)
					continue
				}

			}

		}

	}
	//return nil
}
