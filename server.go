package main

//wallet3test for timeout testing
import (
	"context"
	"fmt"

	"grpcwallet/src/protobuf/protobufpb"
	"grpcwallet/walletoperation"
	"log"
	"net"
	_ "net/http/pprof"
	"time"

	"google.golang.org/grpc/keepalive"

	"github.com/jackc/pgx/v4/pgxpool"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var conn *pgxpool.Pool

func main() {
	//Staging
	// absPath, _ := filepath.Abs("./app/stagingnode.crt")
	// config, err := pgxpool.ParseConfig("postgres://reeturaj:reetu@35.184.195.70:26257/" +
	// 	"isu_db?sslmode=verify-full&sslrootcert=" + absPath)
	config, err := pgxpool.ParseConfig("postgres://reeturaj:reetu@35.184.195.70:26257/" +"isu_db?sslmode=require")

	if err != nil {
		fmt.Println("error configuring the database: ", err)
		log.Fatal("error configuring the database: ", err)
	}
	conn, err = pgxpool.ConnectConfig(context.Background(), config)
	if err != nil {
		fmt.Println("error connecting to the database: ", err)
		log.Fatal("error connecting to the database: ", err)
	}

	defer conn.Close()
	
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	fmt.Println("GRPC server started.")
	
	sp := keepalive.ServerParameters{
		Time:    time.Duration(3 * time.Hour), //10 sec
		Timeout: time.Duration(1 * time.Minute),
		// Time:    time.Duration(10 * time.Second),
		// Timeout: time.Duration(5 * time.Second),
	}
	opts := []grpc.ServerOption{grpc.KeepaliveParams(sp), grpc.MaxConcurrentStreams(500), grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{MinTime: time.Duration(10 * time.Second), PermitWithoutStream: true})}
	grpcServer := grpc.NewServer(opts...)
	//For Credit,Debit,Refund,Status enquiry,Balance Check
	protobufpb.RegisterWalletServiceServer(grpcServer, &walletoperation.Server{Conn: conn})
	//For internal service
	// protobuftest.RegisterServiceWalletServer(grpcServer, &walletoperation.Server{Conn: conn})
	//Register reflection service on gRPC server
	reflection.Register(grpcServer)
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}

}
