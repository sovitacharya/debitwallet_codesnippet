package walletservice

type PubsubRequestData struct {
	Id              string
	PreviousBalance float64 //Same
	Amount          float64 //Same
	CurrentBalance  float64 //Same
	Status          string  //Same
	Type            int     //Same
	WalletId        string  //Same
	ReversalId      string  //Same
	StatusCode      int32   //Same
	Txnid           string
	CreatedDate     string //Same
	UpdatedDate     string //Same
	TransactionType string //Same
}
