package walletservice

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

func InsertWalletledger(ctx context.Context, conn *pgxpool.Conn, insertreq Walletperform) (Walletperform, error) {
	fmt.Println("Inside Insert Wallet:", insertreq.TxnId, "Wallet ID:", insertreq.WalletId, "Api Wallet ID:", insertreq.ApiWalletId, "issl:", insertreq.Is_sl)
	var walletStatus, apiWalletStatus int8
	select1 := time.Now()

	if err := conn.QueryRow(context.Background(), "SELECT status FROM wallet_api WHERE id=$1", insertreq.ApiWalletId).Scan(&walletStatus); err != nil && insertreq.ApiWalletId != 0 {
		fmt.Println("select error", err)
		return insertreq, errors.New("1")
	}
	select1end := time.Since(select1)
	fmt.Println("Select1 Total TIme for Initiation", select1end, "for", insertreq.TxnId)
	select2 := time.Now()
	if err := conn.QueryRow(context.Background(), "SELECT status FROM wallet WHERE id=$1 ", insertreq.WalletId).Scan(&apiWalletStatus); err != nil && insertreq.WalletId != 0 {
		fmt.Println("select error", err)
		return insertreq, errors.New("2")
	}
	select2end := time.Since(select2)
	fmt.Println("Select1 Total TIme for Initiation", select2end, "for", insertreq.TxnId)

	fmt.Println("Inside Insert Wallet::WalletID:", insertreq.WalletId, "Walletstatus:", walletStatus, "And Apiwalletstatus:", apiWalletStatus)

	if insertreq.Is_sl && (walletStatus != 1 && apiWalletStatus == 0) {
		return insertreq, errors.New("3")
	} else if insertreq.Is_sl && (apiWalletStatus != 1 && walletStatus == 0) {
		return insertreq, errors.New("4")
	} else if !insertreq.Is_sl && (walletStatus != 1 || apiWalletStatus != 1) {
		return insertreq, errors.New("5")
	}
	insertreq.CreatedDate = time.Now().Format("2006-01-02 15:04:05.000000")

	insertreq.Status = "INITIATED"
	insertreq.UpdatedDate = insertreq.CreatedDate
	if insertreq.Is_sl && insertreq.ApiWalletId != 0 {
		//For Single Layer API user INSERT in wallet api ledger
		insert1Req := time.Now()
		err := conn.QueryRow(context.Background(),
			"INSERT INTO wallet_api_ledger(previousbalance,amount,currentbalance,walletid,status,type,txnid,reversalid,status_code,createddate,updateddate,transaction_type) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING id", 0, insertreq.Amount, 0, insertreq.ApiWalletId, "INITIATED", insertreq.Type, insertreq.TxnId, insertreq.ApiReversalId, 0, insertreq.CreatedDate, insertreq.CreatedDate, insertreq.TransactionType).Scan(&insertreq.ApiId)
		if err != nil {
			fmt.Println("Inside Insert Wallet::Transaction ID", insertreq.TxnId, "Error for insert ledger", err)
			return insertreq, err
		}
		insert1end := time.Since(insert1Req)
		fmt.Println("api ledger insert time", insert1end)

		// go PubsubData(insertreq)
		return insertreq, nil
	} else if insertreq.Is_sl && insertreq.WalletId != 0 {
		insert2req := time.Now()
		err := conn.QueryRow(context.Background(),
			"INSERT INTO wallet_ledger(previousbalance,amount,currentbalance,walletid,status,type,txnid,reversalid,status_code,createddate,updateddate,transaction_type) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING id", 0, insertreq.Amount, 0, insertreq.WalletId, "INITIATED", insertreq.Type, insertreq.TxnId, insertreq.ReversalId, 0, insertreq.CreatedDate, insertreq.CreatedDate, insertreq.TransactionType).Scan(&insertreq.Id)
		if err != nil {
			fmt.Println("Inside Insert Wallet::Transaction ID", insertreq.TxnId, "Error for insert ledger", err)
			return insertreq, err
		}
		insert2End := time.Since(insert2req)
		fmt.Println("Wallet ledger insert time ", insert2End)
		// go PubsubData(insertreq)
		return insertreq, nil
	} else if !insertreq.Is_sl && insertreq.WalletId != 0 && insertreq.ApiWalletId != 0 {
		insert3req := time.Now()
		err := conn.QueryRow(context.Background(),
			"INSERT INTO wallet_ledger(previousbalance,amount,currentbalance,walletid,status,type,txnid,reversalid,status_code,createddate,updateddate,transaction_type) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING id", 0, insertreq.Amount, 0, insertreq.WalletId, "INITIATED", insertreq.Type, insertreq.TxnId, insertreq.ReversalId, 0, insertreq.CreatedDate, insertreq.CreatedDate, insertreq.TransactionType).Scan(&insertreq.Id)
		// var apiconnnid int64

		// err := conn.QueryRow(context.Background(),
		// 	"INSERT INTO wallet_ledger(previousbalance,amount,currentbalance,walletid,status,type,connnid,reversalid,status_code,createddate,updateddate,transaction_type) VALUES (0,2,0,487,'check',0,987652345678,0,0,'2006-01-02 15:04:05.000000','2006-01-02 15:04:05.000000','check') RETURNING id").Scan(&apiconnnid)

		if err != nil {
			fmt.Println("Inside Insert Wallet::Transaction ID", insertreq.TxnId, "Error for insert ledger", err)
			return insertreq, err
		}
		insert31end := time.Since(insert3req)
		fmt.Println("Wallet ledger for double layer time", insert31end, "for", insertreq.TxnId)
		err = conn.QueryRow(context.Background(),
			"INSERT INTO wallet_api_ledger(previousbalance,amount,currentbalance,walletid,status,type,txnid,reversalid,status_code,createddate,updateddate,transaction_type) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING id", 0, insertreq.Amount, 0, insertreq.ApiWalletId, "INITIATED", insertreq.Type, insertreq.TxnId, insertreq.ApiReversalId, 0, insertreq.CreatedDate, insertreq.CreatedDate, insertreq.TransactionType).Scan(&insertreq.ApiId)
		// var walletconnnid int64
		// err = conn.QueryRow(context.Background(),
		// 	"INSERT INTO wallet_api_ledger(previousbalance,amount,currentbalance,walletid,status,type,connnid,reversalid,status_code,createddate,updateddate,transaction_type) VALUES (0,2,0,487,'check',0,987652345678,0,0,'2006-01-02 15:04:05.000000','2006-01-02 15:04:05.000000','check') RETURNING id").Scan(&walletconnnid)

		if err != nil {
			fmt.Println("Inside Insert Wallet::Transaction ID", insertreq.TxnId, "Error for insert ledger", err)
			return insertreq, err
		}
		// insert32end:=time.Since(insert31end)
		insert3end := time.Since(insert3req)
		fmt.Println("total   double layer time", insert3end, "for", insertreq.TxnId)
		// fmt.Println("insert f")
		// go PubsubData(insertreq)
		return insertreq, nil
	} else {
		fmt.Println("Wrong data send", insertreq.TxnId)
		return insertreq, errors.New("wrong data send")
	}
}
