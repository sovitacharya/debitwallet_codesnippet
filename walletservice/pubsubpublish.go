package walletservice

// import (
// 	"context"
// 	"encoding/json"
// 	"fmt"

// 	"cloud.google.com/go/pubsub"
// 	"github.com/jinzhu/copier"
// )

// type message struct {
// 	Key string            `json:"key"`
// 	Msg PubsubRequestData `json:"message"`
// }

// var (
// 	topic *pubsub.Topic
// )

func PubSubPublish(pubsubreq PubsubRequestData) {
	// ctx := context.Background()
	// //Staging
	// client, err := pubsub.NewClient(ctx, "iserveustaging")
	
	// if err != nil {
	// 	fmt.Println("Errror is:", err)
	// }
	// //Staging topic
	// topicName := "staging_topic"
	// topic = client.Topic(topicName)
	// defer topic.Stop()
	// m := message{
	// 	Key: "wallet1",
	// 	Msg: pubsubreq,
	// }
	// fmt.Println("pubsub Req Data", m)
	// jsonData, err := json.Marshal(m)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// //fmt.Println("Data", string(jsonData))
	// topic.PublishSettings.NumGoroutines = 1
	// result := topic.Publish(ctx, &pubsub.Message{Data: jsonData})
	// id, err := result.Get(ctx)
	// if err != nil {
	// 	fmt.Println("Error in publish", err, "of ID", pubsubreq.Txnid)
	// }
	// fmt.Println("Published a message with msg ID:", id, "of ID", pubsubreq.Txnid)
}

func PubsubData(pubsubdata Walletperform) {
	// if pubsubdata.Is_sl && pubsubdata.Id != 0 {
	// 	pubsubreq := PubsubRequestData{}
	// 	err := copier.Copy(&pubsubreq, &pubsubdata)
	// 	if err != nil {
	// 		fmt.Println("Error in Copy:: Insert request:", err.Error(), "of txn id:", pubsubdata.Id)
	// 	}
	// 	pubsubreq.Id = fmt.Sprint(pubsubdata.Id)
	// 	pubsubreq.Txnid = fmt.Sprint(pubsubdata.TxnId)
	// 	pubsubreq.WalletId = fmt.Sprint(pubsubdata.WalletId)
	// 	pubsubreq.ReversalId = fmt.Sprint(pubsubdata.ReversalId)
	// 	PubSubPublish(pubsubreq)
	// } else if pubsubdata.Is_sl && pubsubdata.ApiId != 0 {
	// 	pubsubreq := PubsubRequestData{}
	// 	err := copier.Copy(&pubsubreq, &pubsubdata)
	// 	if err != nil {
	// 		fmt.Println("Error in Copy:: Insert request:", err.Error(), "of txn id:", pubsubdata.Id)
	// 	}
	// 	pubsubreq.Id = fmt.Sprint(pubsubdata.ApiId)
	// 	pubsubreq.Txnid = fmt.Sprint(pubsubdata.TxnId)
	// 	pubsubreq.WalletId = fmt.Sprint(pubsubdata.ApiWalletId)
	// 	pubsubreq.PreviousBalance = pubsubdata.ApiPreviousBalance
	// 	pubsubreq.CurrentBalance = pubsubdata.ApiCurrentBalance
	// 	pubsubreq.ReversalId = fmt.Sprint(pubsubdata.ApiReversalId)
	// 	PubSubPublish(pubsubreq)

	// } else if !pubsubdata.Is_sl {
	// 	pubsubreq := PubsubRequestData{}
	// 	err := copier.Copy(&pubsubreq, &pubsubdata)
	// 	if err != nil {
	// 		fmt.Println("Error in Copy:: Insert request:", err.Error(), "of txn id:", pubsubdata.Id)
	// 	}
	// 	pubsubreq.Id = fmt.Sprint(pubsubdata.Id)
	// 	pubsubreq.Txnid = fmt.Sprint(pubsubdata.TxnId)
	// 	pubsubreq.WalletId = fmt.Sprint(pubsubdata.WalletId)
	// 	pubsubreq.ReversalId = fmt.Sprint(pubsubdata.ReversalId)
	// 	PubSubPublish(pubsubreq)
	// 	pubsubapireq := PubsubRequestData{}
	// 	apierr := copier.Copy(&pubsubapireq, &pubsubdata)
	// 	if apierr != nil {
	// 		fmt.Println("Error in Copy:: Insert request:", apierr.Error(), "of txn id:", pubsubdata.Id)
	// 	}
	// 	pubsubapireq.Id = fmt.Sprint(pubsubdata.ApiId)
	// 	pubsubapireq.Txnid = fmt.Sprint(pubsubdata.TxnId)
	// 	pubsubapireq.WalletId = fmt.Sprint(pubsubdata.ApiWalletId)
	// 	pubsubapireq.PreviousBalance = pubsubdata.ApiPreviousBalance
	// 	pubsubapireq.CurrentBalance = pubsubdata.ApiCurrentBalance
	// 	pubsubapireq.ReversalId = fmt.Sprint(pubsubdata.ApiReversalId)
	// 	PubSubPublish(pubsubapireq)
	// }

}
