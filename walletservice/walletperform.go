package walletservice

type Walletperform struct {
	Id                 int64
	ApiId              int64
	PreviousBalance    float64
	CurrentBalance     float64
	ApiPreviousBalance float64
	ApiCurrentBalance  float64
	WalletId           int64
	ApiWalletId        int64
	Amount             float64
	Is_sl              bool
	Type               int
	ReversalId         int64
	ApiReversalId      int64
	TxnId              int64
	Status             string
	StatusCode         int32
	CreatedDate        string
	UpdatedDate        string
	TransactionType    string
	MaximumBalance     float64
	MinimumBalance     float64
	ApimaximumBalnce   float64
	ApiminimumBalance  float64
}
