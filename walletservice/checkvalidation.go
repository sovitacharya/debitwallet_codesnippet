package walletservice

import (
	"errors"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
)

func Checkvalidationforwallet(request *protobufpb.Request) (*protobufpb.Response, error) {
	response := &protobufpb.Response{}
	response.Txnid = request.Txnid
	response.Walletstatus = "FAILED"
	response.Wallettransactionid = 0
	response.Apiwallettransactionid = 0
	response.Walletstatuscode = 8
	if request.IsSl && ((request.Apiwalletid != 0 && request.Walletid != 0) || (request.Apiwalletid == 0 && request.Walletid == 0)) {
		response.Walletstatusdesc = "In single layer,One wallet ID is mandatory."
		return response, errors.New("in single layer,One wallet id is mandatory")
	} else if !request.IsSl && (request.Walletid == 0 || request.Apiwalletid == 0) {
		response.Walletstatusdesc = "In Double layer,Both wallet ID is mandatory."
		return response, errors.New("in double layer,both wallet id is mandatory")
	} else if request.Amount <= 0 {
		response.Walletstatusdesc = "Amount should be greater than 0."
		return response, errors.New("amount should be greater than 0")
	} else if request.Txnid <= 0 {
		response.Walletstatusdesc = "Txn ID should not be null or zero or -ve"
		return response, errors.New("txn id should not be null or zero or -ve")
	} else if request.TransactionType == "" {
		response.Walletstatusdesc = "Transaction type should not be NULL"
		return response, errors.New("transaction type should not be null")
	}
	return response, nil
}
