package main

import (
	"context"

	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"grpcwallet/src/protobuf/protobufpb"
	"io"

	"testing"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)
var client protobufpb.WalletServiceClient
type Testcase struct { want int64 }
func init() {
	var host = "wallet3test-twdwtabx5q-uc.a.run.app:443"

	var opts []grpc.DialOption
	if host != "" {
		opts = append(opts, grpc.WithAuthority(host))
	}
	systemRoots, err := x509.SystemCertPool()
	if err != nil {
		fmt.Println("Error in system certpool", err)
	}
	cred := credentials.NewTLS(&tls.Config{
		RootCAs: systemRoots,
	})
	opts = append(opts, grpc.WithTransportCredentials(cred))

	cc, err := grpc.Dial(host, opts...)
	if err != nil {
		fmt.Println("dial error", err)
	}
    client = protobufpb.NewWalletServiceClient(cc)
}
func TestDebitWallet0(t *testing.T) {
	tt := Testcase{}
	tt.want=0
	stream, err := client.DebitWallet(context.Background())

	if err != nil {
		t.Errorf("conn err =%v", err)
	}
	i,_:= rand.Prime(rand.Reader, 48)
	r := &protobufpb.Request{
		Amount:      1,
		Walletid:    487,
		Apiwalletid: 3,
		Txnid: i.Int64(),
		IsSl:  false,
		TransactionType: "TEST_WALLET",
	}
	fmt.Println("Sending Message:", r)
	stream.Send(r)
	stream.CloseSend()
	for {
		start := time.Now()
		res, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			t.Errorf("rcv err =%v", err)
			break
		}
		fmt.Println("Per req", time.Since(start))
		fmt.Printf("Received: %v\n ", res)
		if res.Walletstatuscode != int32(tt.want)  {
			t.Errorf("Debit status =%v, wanted %v",res.Walletstatuscode, tt.want )
		}else{
			fmt.Printf("t:passed \n")
		}
	}
}

